#pragma once

#include <string>
#include <algorithm>
#include <unistd.h>
#include <curl/curl.h>

#include "api_key.hh"

std::string make_url(std::string location, int heading);
void do_download(std::string url, std::string name);
