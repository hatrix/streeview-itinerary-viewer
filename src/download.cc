#include "download.hh"

void replaceAll(std::string& str, const std::string& from, const std::string& to) {
	if(from.empty())
    return;
	size_t start_pos = 0;
	while((start_pos = str.find(from, start_pos)) != std::string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
	}
}

std::string make_url(std::string location, int heading)
{
	replaceAll(location, " ", "%20");

  std::string url = "https://maps.googleapis.com/maps/api/streetview?";
  url += "size=800x450&";
  url += "location=" + location + "&";
  url += "fov=120&";
  url += "heading=" + std::to_string(heading) + "&";
  url += "pitch=-5&";
  url += "key=" + g_api_key_street;

  return url;
}

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}

void do_download(std::string url, const std::string name)
{
  CURL *curl_handle;
  static const char *pagefilename = name.c_str();
  FILE *pagefile;

  curl_global_init(CURL_GLOBAL_ALL);
  curl_handle = curl_easy_init();

	// URL C style
  curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());

  curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);

  pagefile = fopen(pagefilename, "wb");
  if(pagefile) {
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);
    curl_easy_perform(curl_handle);

    fclose(pagefile);
  }
  
	curl_easy_cleanup(curl_handle);
}
