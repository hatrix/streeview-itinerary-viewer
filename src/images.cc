#include "images.hh"

void sphere(std::string location, std::string base_path)
{
  std::string url; 
  std::string name;
  std::string suffix;

  for (int i = 0; i < 360; i+=5)
  {
    url = make_url(location, i);

    if (i < 10)
      suffix = std::string(2, '0');
    else if (i < 100)
      suffix = std::string(1, '0');
    else
      suffix = "";

    name = base_path + "_" + suffix + std::to_string(i) + ".jpg";

    do_download(url, name);
  }
}
