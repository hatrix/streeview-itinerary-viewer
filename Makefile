CC=g++
CFLAGS=-Wall -Wextra -pedantic -std=c++17 -g
LDFLAGS=-lcurl

BIN=viewer
SRC=src/main.cc src/images.cc src/download.cc

all: $(SRC)
	$(CC) $(CFLAGS) -o $(BIN) $^ $(LDFLAGS)

gif:
	ffmpeg -framerate 10 -pattern_type glob -i '*.jpg' -c:v libx264 -pix_fmt yuv420p out.mp4 -y
	convert out.mp4 animated.gif

clean:
	rm -f $(BIN)
	rm -f out.mp4
	rm -f *.jpg
